#!/usr/bin/env php
<?php

use Drupal\swoole\Actions\EnsureRequestsDontExceedMaxExecutionTime;
use Drupal\swoole\Client;
use Drupal\swoole\DrupalKernelFactory;
use Drupal\swoole\Extension;
use Drupal\swoole\Stream;
use Drupal\swoole\Tables\TableFactory;
use Drupal\swoole\Worker;
use Drupal\swoole\WorkerState;
use Swoole\Http\Server;
use Swoole\Table;
use Swoole\Timer;

// cspell:ignore swoole Swoole bootstrapper SWOOLE managerstart workerstart
// cspell:ignore workerstop Dont

// @see Drupal\Core\DrupalKernel::bootEnvironment()
// Use session cookies, not transparent sessions that puts the session id
// in the query string.
ini_set('session.use_cookies', '1');
ini_set('session.use_only_cookies', '1');
ini_set('session.use_trans_sid', '0');
// Don't send HTTP headers using PHP's session handler.
// Send an empty string to disable the cache limiter.
ini_set('session.cache_limiter', '');
// Use httponly session cookies.
ini_set('session.cookie_httponly', '1');

ini_set('display_errors', 'stderr');

/**
 * --------------------------------------------------------------------------
 * Find Application Path To The File Settings.php
 * --------------------------------------------------------------------------
 *
 * First we need to locate the path to the application bootstrapper, which
 * is able to create a fresh copy of the Drupal site for us and
 * we can use this to handle requests. For now we just need the path.
 *
 */
$drupal_root = $_ENV['DRUPAL_ROOT'] ?? NULL;
if (!is_string($drupal_root)) {
  fwrite(STDERR, 'The variable with the web root directory for Drupal is not set.');
  exit();
}
if (!is_dir($drupal_root)) {
  fwrite(STDERR, 'The given web root directory for Drupal does not exists.');
  exit();
}
$site_path = $_ENV['SITE_PATH'] ?? NULL;
if (!is_string($site_path)) {
  fwrite(STDERR, 'The variable with the site path for Drupal is not set.');
  exit();
}
if (!is_file($settings_file = $drupal_root . '/' . $site_path . '/settings.php')) {
  fwrite(STDERR, 'The settings.php file does not exists.');
  exit();
}

// Change the current dir to DRUPAL_ROOT.
chdir($drupal_root);

/**
 * --------------------------------------------------------------------------
 * Register The Auto Loader
 * --------------------------------------------------------------------------
 *
 * Composer provides a convenient, automatically generated class loader
 * for our application. We just need to utilize it! We'll require it
 * into the script here so that we do not have to worry about the
 * loading of any our classes "manually". Feels great to relax.
 *
 */
if (!is_file($autoload_file = $drupal_root.'/vendor/autoload.php')) {
  fwrite(STDERR, "Composer autoload file was not found. Did you install the project's dependencies?".PHP_EOL);
  exit();
}

$autoloader = require_once $autoload_file;

/**
 *--------------------------------------------------------------------------
 * Create The Swoole Server
 *--------------------------------------------------------------------------
 *
 * First, we will load the server state file from disk. This file contains
 * various information we need to boot Swoole such as the configuration
 * and application name. We can use this data to start up our server.
 *
 */
$ssl = ($_ENV['SWOOLE_SSL'] === 'TRUE') ? TRUE : FALSE;
try {
  // Create a new Swoole server.
  $server = new Server(
    $_ENV['SWOOLE_HOST'],
    $_ENV['SWOOLE_PORT'],
    SWOOLE_PROCESS,
    ($ssl ? SWOOLE_SOCK_TCP | SWOOLE_SSL : SWOOLE_SOCK_TCP),
  );

//throw new \Exception('Iago.');
}
catch (\Throwable $e) {
  Stream::throwable($e);
  exit();
}

$extension = new Extension();
$server->set([
  'enable_coroutine' => TRUE,
  'send_yield' => TRUE,
  'daemonize' => ($_ENV['SWOOLE_DAEMONIZE'] === 'TRUE') ? TRUE : FALSE,

  'ssl_cert_file' => $ssl ? $drupal_root . '/' . $_ENV['SWOOLE_SSL_CERT_FILE'] : '',
  'ssl_key_file' => $ssl ? $drupal_root . '/' . $_ENV['SWOOLE_SSL_KEY_FILE'] : '',

  // Change the size in MB to bytes.
  'package_max_length' => ($_ENV['SWOOLE_PACKAGE_MAX_LENGTH']) * 1024 * 1024,
  'socket_buffer_size' => ($_ENV['SWOOLE_SOCKET_BUFFER_SIZE']) * 1024 * 1024,

  'reactor_num' => $extension->cpuCount(),

  'worker_num' => $_ENV['SWOOLE_WORKER_NUM'] ?? $extension->cpuCount(),
  'max_request' => $_ENV['SWOOLE_MAX_REQUEST'],

  'task_worker_num' => $_ENV['SWOOLE_TASK_WORKER_NUM'] ?? $extension->cpuCount(),
  'task_max_request' => $_ENV['SWOOLE_TASK_MAX_REQUEST'],

  'log_level' => $_ENV['SWOOLE_LOG_LEVEL'],
  'log_file' => $_ENV['SWOOLE_LOG_FILE'],
]);
$site_name = $_ENV['SITE_NAME'] ?? 'Drupal';
$ticks = ($_ENV['SWOOLE_TICKS'] === 'TRUE') ? TRUE : FALSE;

/**
 *--------------------------------------------------------------------------
 * Create The Timer Table
 *--------------------------------------------------------------------------
 *
 */
$timer_table = NULL;
$max_execution_time = $_ENV['SWOOLE_MAX_EXECUTION_TIME'] ?? 0;
if ($max_execution_time > 0) {
  $timer_table = TableFactory::make(250);
  $timer_table->column('worker_pid', Table::TYPE_INT);
  $timer_table->column('time', Table::TYPE_INT);
  $timer_table->create();
}

$workerState = new WorkerState();
$workerState->timer_table = $timer_table;

/**
 * --------------------------------------------------------------------------
 * Handle Server & Manager Start
 * --------------------------------------------------------------------------
 *
 * The following callbacks manage the master process and manager process
 * start events. These handlers primarily are responsible for writing
 * the process ID to the server state file so we can remember them.
 *
 */
$server->on('start', function ($server) use ($site_name, $ticks, $timer_table, $max_execution_time) {
  $extension = new Extension();
  $extension->setProcessName($site_name, 'master process');

  if ($ticks) {
    $server->tick(1000, function () use ($server) {
      $server->task('swoole-tick');
    });
  }

  if (($max_execution_time > 0) && $timer_table) {
    $server->tick(1000, function () use ($extension, $timer_table, $max_execution_time) {
      (new EnsureRequestsDontExceedMaxExecutionTime($extension, $timer_table, $max_execution_time))();
    });
  }
});

$server->on('managerstart', function ($server) use ($site_name) {
  $extension = new Extension();
  $extension->setProcessName($site_name, 'manager process');
});

/**
 * --------------------------------------------------------------------------
 * Handle Worker Start
 * --------------------------------------------------------------------------
 *
 * Swoole will start multiple worker processes and the following callback
 * will handle their state events. When a worker starts we will create
 * a new Swoole worker and inform it to start handling our requests.
 *
 * We will also create a "workerState" variable which will maintain state
 * and allow us to access the worker and client from the callback that
 * will handle incoming requests. Basically this works like a cache.
 *
 */
$server->on('workerstart', function ($server, $workerId) use ($workerState, $autoloader, $drupal_root, $site_path, $site_name) {
  $stream_to_console = ($_ENV['SWOOLE_STREAM_TO_CONSOLE'] === 'TRUE') ? TRUE : FALSE;
  $add_memory_usage = ($_ENV['SWOOLE_ADD_MEMORY_USAGE'] === 'TRUE') ? TRUE : FALSE;
  $add_handle_duration = ($_ENV['SWOOLE_ADD_HANDLE_DURATION'] === 'TRUE') ? TRUE : FALSE;

  $workerState->server = $server;
  $workerState->workerId = $workerId;
  $workerState->workerPid = posix_getpid();

  try {
    $factory = new DrupalKernelFactory($autoloader, $drupal_root, $site_path, $server);
    $kernel = $factory->createDrupalKernel();
    $client = new Client();
    $workerState->worker = new Worker($kernel, $client);
    $workerState->worker->boot();
    $workerState->worker->streamRequestDataToConsoleSettings($stream_to_console, $add_memory_usage, $add_handle_duration);
  }
  catch (\Throwable $e) {
    Stream::throwable($e);
    $server->shutdown();
  }

  $isTaskWorker = $workerId >= $server->setting['worker_num'];
  $extension = new Extension();
  $extension->setProcessName($site_name, $isTaskWorker ? 'task worker process' : 'worker process');
});

/**
 * --------------------------------------------------------------------------
 * Handle Incoming Requests
 * --------------------------------------------------------------------------
 *
 * The following callback will handle all incoming requests plus send them
 * the worker. The worker will send the request through the application
 * and ask the client to send the response back to the Swoole server.
 *
 */
$server->on('request', function ($swoole_request, $swoole_response) use ($drupal_root, $workerState) {
  $drupal_request = (new \Drupal\swoole\Actions\ConvertSwooleRequestToDrupalRequest)($swoole_request);
  $workerState->worker->handle($drupal_request, $swoole_response, $drupal_root);
});

/**
 * --------------------------------------------------------------------------
 * Handle Tasks
 *--------------------------------------------------------------------------
 *
 * Swoole tasks can be used to offload concurrent work onto a group of
 * background processes which handle the work in isolation and with
 * separate application state. We should handle these tasks below.
 *
 */
$server->on('task', function (Server $server, int $taskId, int $fromWorkerId, $data) use ($workerState) {
  if ($data === 'swoole-tick') {
    $workerState->worker->handleTick();
  }
  else {
    $workerState->worker->handleTask($data);
  }
});

/**
 * --------------------------------------------------------------------------
 * Handle Worker & Server Shutdown
 * --------------------------------------------------------------------------
 *
 * The following callbacks handle the master and worker shutdown events so
 * we can clean up any state, including the server state file. An event
 * will be dispatched by the worker so the developer can take action.
 *
 */
$server->on('workerstop', function () use ($workerState) {
  if ($workerState->tickTimerId) {
    Timer::clear($workerState->tickTimerId);
  }

  $workerState->worker->terminate();
});

$server->start();
