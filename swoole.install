<?php

/**
 * @file
 * Install, update and uninstall functions for the swoole module.
 */

// cspell:ignore swoole Swoole openswoole

/**
 * Implements hook_requirements().
 */
function swoole_requirements($phase) {
  $requirements = [];
  if ($phase === 'runtime') {
    // Make sure that the Server Operating system is Linux as the (Open)Swoole
    // PHP extension only works on Linux.
    $requirements['swoole_linux'] = [
      'title' => t('Swoole on Linux'),
      'description' => t("The (Open)Swoole PHP extension only works on Linux."),
    ];
    if (PHP_OS_FAMILY === 'Linux') {
      $requirements['swoole_linux']['value'] = t('The OS is Linux.');
      $requirements['swoole_linux']['severity'] = REQUIREMENT_OK;
    }
    else {
      $requirements['swoole_linux']['value'] = t('The OS is not Linux.');
      $requirements['swoole_linux']['severity'] = REQUIREMENT_ERROR;
    }

    // Make sure that the Swoole PHP extension OR the OpenSwoole PHP extension
    // is installed.
    $requirements['swoole_extension'] = [
      'title' => t('Swoole extension'),
      'description' => t("The Swoole module needs the Swoole PHP extension OR the OpenSwoole PHP extension in order to work."),
    ];
    if (!extension_loaded('swoole') && !extension_loaded('openswoole')) {
      $requirements['swoole_extension']['value'] = t('No (Open)Swoole PHP extension is installed.');
      $requirements['swoole_extension']['severity'] = REQUIREMENT_ERROR;
    }
    elseif (extension_loaded('swoole') && extension_loaded('openswoole')) {
      $requirements['swoole_extension']['value'] = t('Both the Swoole PHP extension and the OpenSwoole PHP extension are installed.');
      $requirements['swoole_extension']['severity'] = REQUIREMENT_ERROR;
    }
    elseif (extension_loaded('swoole') && !extension_loaded('openswoole')) {
      $requirements['swoole_extension']['value'] = t('The Swoole PHP extension is installed.');
      $requirements['swoole_extension']['severity'] = REQUIREMENT_OK;
    }
    elseif (!extension_loaded('swoole') && extension_loaded('openswoole')) {
      $requirements['swoole_extension']['value'] = t('The OpenSwoole PHP extension is installed.');
      $requirements['swoole_extension']['severity'] = REQUIREMENT_OK;
    }

    // Make sure that the Big Pipe module is not installed. It does not work in
    // combination with the Swoole module.
    // @see https://openswoole.com/docs/modules/swoole-http-response.
    $requirements['swoole_big_pipe'] = [
      'title' => t('Swoole module with Big pipe module'),
      'description' => t("The Swoole PHP extension does not work with the Big Pipe module. The Big Pipe module adds stuff to the response that the Swoole response does not allow. The Swoole response is a little more basic in what is allowed."),
    ];
    if (\Drupal::service('module_handler')->moduleExists('big_pipe')) {
      $requirements['swoole_big_pipe']['value'] = t('The Big Pipe module is enabled.');
      $requirements['swoole_big_pipe']['severity'] = REQUIREMENT_ERROR;
    }
    else {
      $requirements['swoole_big_pipe']['value'] = t('The Big Pipe module is not enabled.');
      $requirements['swoole_big_pipe']['severity'] = REQUIREMENT_OK;
    }
  }

  return $requirements;
}
