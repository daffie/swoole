<?php

namespace Drupal\swoole;

// cspell:ignore swoole Swoole SIGTERM

/**
 * The Swoole signal dispatcher.
 */
class SignalDispatcher {

  /**
   * The Swoole extension service.
   *
   * @var \Drupal\swoole\Extension
   */
  protected $extension;

  /**
   * Constructs a SignalDispatcher object.
   *
   * @param \Drupal\swoole\Extension $extension
   *   The dispatcher service.
   */
  public function __construct(Extension $extension) {
    $this->extension = $extension;
  }

  /**
   * Determine if the given process ID can be communicated with.
   *
   * @param int $process_id
   *   The process identifier.
   *
   * @return bool
   *   The result of the signal.
   */
  public function canCommunicateWith(int $process_id): bool {
    return $this->signal($process_id, 0);
  }

  /**
   * Send a SIGTERM signal to the given process.
   *
   * @param int $process_id
   *   The process identifier.
   * @param int $wait
   *   The wait time.
   *
   * @return bool
   *   The result of the signal.
   */
  public function terminate(int $process_id, int $wait = 0): bool {
    $this->extension->dispatchProcessSignal($process_id, SIGTERM);

    if ($wait) {
      $start = time();

      do {
        if (!$this->canCommunicateWith($process_id)) {
          return TRUE;
        }

        $this->extension->dispatchProcessSignal($process_id, SIGTERM);

        sleep(1);
      } while (time() < $start + $wait);
    }

    return FALSE;
  }

  /**
   * Send a signal to the given process.
   *
   * @param int $process_id
   *   The process identifier.
   * @param int $signal
   *   The signal type.
   *
   * @return bool
   *   The result of the signal.
   */
  public function signal(int $process_id, int $signal): bool {
    return $this->extension->dispatchProcessSignal($process_id, $signal);
  }

}
