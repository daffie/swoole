<?php

namespace Drupal\swoole;

use Drupal\Core\DrupalKernel;
use Swoole\Http\Response as SwooleResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// cspell:ignore swoole Swoole

/**
 * The interface for communicating with the client.
 */
interface ClientInterface {

  /**
   * Send the response to the server.
   *
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param \Symfony\Component\HttpFoundation\Response $drupal_response
   *   The Drupal response.
   * @param string|null $output_buffer
   *   The output buffer.
   */
  public function respond(SwooleResponse $swoole_response, Response $drupal_response, ?string $output_buffer = NULL): void;

  /**
   * Send an error message to the server.
   *
   * @param \Throwable $e
   *   The thrown exception.
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   */
  public function error(\Throwable $e, DrupalKernel $kernel, Request $drupal_request, SwooleResponse $swoole_response): void;

}
