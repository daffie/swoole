<?php

namespace Drupal\swoole;

use Drupal\Core\DrupalKernel;

// cspell:ignore swoole

/**
 * The trait for dispatching events.
 */
trait DispatchesEvents {

  /**
   * Dispatch the given event via the given Drupal kernel instance.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param mixed $event
   *   The event.
   */
  public function dispatchEvent(DrupalKernel $kernel, $event): void {
    if ($kernel->getContainer()->has('event_dispatcher')) {
      $kernel->getContainer()->get('event_dispatcher')->dispatch($event);
    }
  }

}
