<?php

namespace Drupal\swoole;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\Process\Process;

// cspell:ignore swoole Swoole pids SIGKILL pgrep

/**
 * The Swoole server process inspector.
 */
class ServerProcessInspector {

  /**
   * The config.factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The signal dispatcher service.
   *
   * @var \Drupal\swoole\SignalDispatcher
   */
  protected $dispatcher;

  /**
   * Constructs a ServerProcessInspector object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service.
   * @param \Drupal\swoole\SignalDispatcher $dispatcher
   *   The dispatcher service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, SignalDispatcher $dispatcher) {
    $this->configFactory = $configFactory;
    $this->dispatcher = $dispatcher;
  }

  /**
   * Determine if the Swoole server process is running.
   *
   * @return bool
   */
  public function serverIsRunning(): bool {
    $settings = $this->configFactory->getEditable('swoole.settings');
    $master_pid = $settings->get('master_pid') ?? -1;
    $manager_pid = $settings->get('manager_pid') ?? -1;

    if (($master_pid > 0) && ($manager_pid === -1)) {
      $manager_pids = $this->getChildProcessIds($master_pid);
      if (count($manager_pids) == 1) {
        $manager_pid = reset($manager_pids);
        if ($manager_pid > 0) {
          $settings->set('manager_pid', (int) $manager_pid)->save(TRUE);
        }
      }
    }

    return $manager_pid
      ? $master_pid && $manager_pid && $this->dispatcher->canCommunicateWith((int) $manager_pid)
      : $master_pid && $this->dispatcher->canCommunicateWith((int) $master_pid);
  }

  /**
   * Stop the Swoole server.
   *
   * @return bool
   */
  public function stopServer(): bool {
    $settings = $this->configFactory->getEditable('swoole.settings');
    $master_pid = $settings->get('master_pid') ?? -1;

    // Nothing to do when there is no master process ID set.
    if ($master_pid === -1) {
      return TRUE;
    }

    // Get the manager process ID.
    $manager_pid = $settings->get('manager_pid') ?? -1;
    if ($manager_pid === -1) {
      $manager_pids = $this->getChildProcessIds($master_pid);
      $manager_pid = reset($manager_pids);
    }

    // Get all the worker process IDs.
    $worker_pids = $this->getChildProcessIds($manager_pid);

    // Stop all the processes related to the master process ID.
    foreach ([$master_pid, $manager_pid, ...$worker_pids] as $process_id) {
      $this->dispatcher->signal($process_id, SIGKILL);
    }

    // The Swoole server has been stopped. Reset the master and manager process
    // IDs.
    $settings->set('master_pid', -1)->save(TRUE);
    $settings->set('manager_pid', -1)->save(TRUE);

    return TRUE;
  }

  /**
   * Helper method to get the child process IDs.
   *
   * @param string|int $parent_pid
   *   The parent process ID.
   *
   * @return array
   *   Returns an array with integer child process IDs.
   */
  protected function getChildProcessIds($parent_pid): array {
    $child_pids = [];
    $process = new Process(['pgrep', '-P', (string) $parent_pid]);
    $process->run();
    if ($process->isSuccessful()) {
      foreach ($process as $type => $data) {
        if ($process::OUT === $type) {
          $data = str_replace(PHP_EOL, ' ', $data);
          $pids = explode(' ', $data);
          foreach ($pids as $pid) {
            $pid = (int) $pid;
            if ($pid > 0) {
              $child_pids[] = $pid;
            }
          }
        }
      }
    }
    return $child_pids;
  }

}
