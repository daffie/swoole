<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

// cspell:ignore swoole

/**
 * The interface for terminated events.
 */
interface OperationTerminatedInterface {

  /**
   * Get the base kernel instance.
   *
   * @return \Drupal\Core\DrupalKernel
   */
  public function kernel(): DrupalKernel;

  /**
   * Get the sandbox version of the kernel instance.
   *
   * @return \Drupal\Core\DrupalKernel
   */
  public function sandbox(): DrupalKernel;

}
