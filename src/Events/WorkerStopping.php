<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

// cspell:ignore swoole

/**
 * The worker stopped event.
 */
class WorkerStopping {

  /**
   * Constructs a WorkerStopping object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   */
  public function __construct(
    public DrupalKernel $kernel
  ) {}

}
