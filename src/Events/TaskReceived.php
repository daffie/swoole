<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

// cspell:ignore swoole sandboxed recieved Recieved

/**
 * The task recieved event.
 */
class TaskReceived {

  /**
   * Constructs a TaskRecieved object.
   *
   * @param \Drupal\Core\DrupalKernel $kernel
   *   The Drupal kernel.
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   * @param mixed $data
   *   The data for the event
   */
  public function __construct(
    public DrupalKernel $kernel,
    public DrupalKernel $sandbox,
    public $data
  ) {}

}
