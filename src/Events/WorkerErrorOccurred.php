<?php

namespace Drupal\swoole\Events;

use Drupal\Core\DrupalKernel;

// cspell:ignore swoole sandboxed

/**
 * The worker error occurred event.
 */
class WorkerErrorOccurred {

  /**
   * Constructs a WorkerErrorOccurred object.
   *
   * @param \Throwable $exception
   *   The thrown exception.
   * @param \Drupal\Core\DrupalKernel $sandbox
   *   The sandboxed Drupal kernel.
   */
  public function __construct(
    public \Throwable $exception,
    public DrupalKernel $sandbox
  ) {}

}
