<?php

namespace Drupal\swoole;

use Drupal\Core\Utility\Error;

// cspell:ignore swoole

/**
 * Stream swoole data to the console.
 */
class Stream {

  /**
   * Stream the given request information to stdout.
   *
   * @param string $method
   *   The used method.
   * @param string $url
   *   The used URL.
   * @param int $memory
   *   The size of the used memory.
   * @param float $duration
   *   The duration.
   */
  public static function request(string $method, string $url, int $memory, float $duration): void {
    fwrite(STDOUT, json_encode([
      'stream_type' => 'request',
      'method' => $method,
      'url' => $url,
      'memory' => $memory,
      'duration' => $duration,
    ]) . "\n");
  }

  /**
   * Stream the given throwable to stderr.
   *
   * @param \Throwable $throwable
   *   The thrown exception.
   */
  public static function throwable(\Throwable $throwable): void {
    $error = Error::decodeException($throwable);
    $error['stream_type'] = 'throwable';
    fwrite(STDERR, json_encode($error) . "\n");
  }

}
