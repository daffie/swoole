<?php

namespace Drupal\swoole\Actions;

use Drupal\swoole\Extension;

// cspell:ignore swoole Swoole SIGKILL Dont

/**
 * Stop worker processes that exceed the maximum execution time.
 */
class EnsureRequestsDontExceedMaxExecutionTime {

  /**
   * Constructs a EnsureRequestsDontExceedMaxExecutionTime object.
   *
   * @param \Drupal\swoole\Extension $extension
   *   The Swoole extension.
   * @param array $timerTable
   *   The time table.
   * @param int $maxExecutionTime
   *   The maximum execution time.
   */
  public function __construct(
    protected Extension $extension,
    protected $timerTable,
    protected $maxExecutionTime
  ) {}

  /**
   * Invoke the action.
   */
  public function __invoke(): void {
    foreach ($this->timerTable as $worker_id => $row) {
      if ((time() - $row['time']) > $this->maxExecutionTime) {
        $this->timerTable->del($worker_id);

        $this->extension->dispatchProcessSignal($row['worker_pid'], SIGKILL);
      }
    }
  }

}
