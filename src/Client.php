<?php

namespace Drupal\swoole;

use Drupal\Core\DrupalKernel;
use Swoole\Http\Response as SwooleResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

// cspell:ignore swoole Swoole rawcookie

/**
 * Communicate the response to the client.
 */
class Client implements ClientInterface, ServesStaticFilesInterface {

  const STATUS_CODE_REASONS = [
    419 => 'Page Expired',
  ];

  /**
   * Constructs a TaskResult object.
   *
   * @param int $chunkSize
   *   The size of the chunk.
   */
  public function __construct(
    protected int $chunkSize = 1048576
  ) {}

  /**
   * Determine if the request can be served as a static file.
   *
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   *
   * @return bool
   *   Can the static file be served.
   */
  public function canServeRequestAsStaticFile(Request $drupal_request, string $drupal_root): bool {
    if (!$drupal_root || $drupal_request->getPathInfo() === '/') {
      return FALSE;
    }

    $path_to_file = realpath($drupal_root . '/' . $drupal_request->getPathInfo());

    if ($this->isValidFileWithinSymlink($drupal_request, $drupal_root, $path_to_file)) {
      $path_to_file = $drupal_root . '/' . $drupal_request->getPathInfo();
    }

    return $this->fileIsServable($drupal_root, $path_to_file);
  }

  /**
   * Determine if the request is for a valid static file within a symlink.
   *
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   * @param string $path_to_file
   *   Absolute path to the file.
   *
   * @return bool
   *   Whether the static file is within the symlink.
   */
  private function isValidFileWithinSymlink(Request $drupal_request, string $drupal_root, string $path_to_file): bool {
    $path_after_symlink = $this->pathAfterSymlink($drupal_root, $drupal_request->getPathInfo());

    return $path_after_symlink && str_ends_with($path_to_file, $path_after_symlink);
  }

  /**
   * If the given public file is within a symlinked directory, return the path after the symlink.
   *
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   * @param string $path
   *   The relative path to the directory.
   *
   * @return string|false
   *   The symlinked directory or FALSE.
   */
  private function pathAfterSymlink(string $drupal_root, string $path) {
    $directories = explode('/', $path);

    while ($directory = array_shift($directories)) {
      $drupal_root .= '/' . $directory;

      if (is_link($drupal_root)) {
        return implode('/', $directories);
      }
    }

    return FALSE;
  }

  /**
   * Determine if the given file is servable.
   *
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   * @param string $path_to_file
   *   Absolute path to the file.
   *
   * @return bool
   *   Whether the file is servable.
   */
  protected function fileIsServable(string $drupal_root, string $path_to_file): bool {
    return $path_to_file &&
      !in_array(pathinfo($path_to_file, PATHINFO_EXTENSION), ['php', 'htaccess', 'config']) &&
      str_starts_with($path_to_file, $drupal_root) &&
      is_file($path_to_file);
  }

  /**
   * Serve the static file that was requested.
   *
   * @param \Symfony\Component\HttpFoundation\Request $drupal_request
   *   The Drupal request.
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   */
  public function serveStaticFile(Request $drupal_request, SwooleResponse $swoole_response, string $drupal_root): void {
    $swoole_response->status(200);
    $swoole_response->header('Content-Type', MimeType::get(pathinfo($drupal_request->getPathInfo(), PATHINFO_EXTENSION)));
    $swoole_response->sendfile(realpath($drupal_root . '/' . $drupal_request->getPathInfo()));
  }

  /**
   * {@inheritdoc}
   */
  public function respond(SwooleResponse $swoole_response, Response $drupal_response, ?string $output_buffer = NULL): void {
    $this->sendResponseHeaders($swoole_response, $drupal_response);
    $this->sendResponseContent($swoole_response, $drupal_response, $output_buffer);
  }

  /**
   * Send the headers from the Drupal response to the Swoole response.
   *
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param \Symfony\Component\HttpFoundation\Response $drupal_response
   *   The Drupal response.
   */
  public function sendResponseHeaders(SwooleResponse $swoole_response, Response $drupal_response): void {
    if (!$drupal_response->headers->has('Date')) {
      $drupal_response->setDate(\DateTime::createFromFormat('U', time()));
    }

    $headers = $drupal_response->headers->allPreserveCase();

    if (isset($headers['Set-Cookie'])) {
      unset($headers['Set-Cookie']);
    }

    foreach ($headers as $name => $values) {
      foreach ($values as $value) {
        $swoole_response->header($name, $value);
      }
    }

    if (!is_null($reason = $this->getReasonFromStatusCode($drupal_response->getStatusCode()))) {
      $swoole_response->status($drupal_response->getStatusCode(), $reason);
    }
    else {
      $swoole_response->status($drupal_response->getStatusCode());
    }

    foreach ($drupal_response->headers->getCookies() as $cookie) {
      $swoole_response->{$cookie->isRaw() ? 'rawcookie' : 'cookie'}(
        $cookie->getName(),
        $cookie->getValue() ?? '',
        $cookie->getExpiresTime(),
        $cookie->getPath(),
        $cookie->getDomain() ?? '',
        (bool) $cookie->isSecure(),
        (bool) $cookie->isHttpOnly(),
        $cookie->getSameSite() ?? '',
      );
    }
  }

  /**
   * Send the content from the Drupal response to the Swoole response.
   *
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param \Symfony\Component\HttpFoundation\Response $drupal_response
   *   The Drupal response.
   * @param string|null $output_buffer
   *   The output buffer.
   */
  protected function sendResponseContent(SwooleResponse $swoole_response, Response $drupal_response, ?string $output_buffer = NULL): void {
    if ($drupal_response instanceof BinaryFileResponse) {
      $swoole_response->sendfile($drupal_response->getFile()->getPathname());

      return;
    }

    if ($output_buffer) {
      $swoole_response->write($output_buffer);
    }

    if ($drupal_response instanceof StreamedResponse) {
      ob_start(function ($data) use ($swoole_response) {
        if (strlen($data) > 0) {
          $swoole_response->write($data);
        }

        return '';
      }, 1);

      $drupal_response->sendContent();

      ob_end_clean();

      $swoole_response->end();

      return;
    }

    $content = $drupal_response->getContent();

    if (($length = strlen($content)) === 0) {
      $swoole_response->end();

      return;
    }

    if ($length <= $this->chunkSize) {
      $swoole_response->write($content);
    }
    else {
      for ($offset = 0; $offset < $length; $offset += $this->chunkSize) {
        $swoole_response->write(substr($content, $offset, $this->chunkSize));
      }
    }

    $swoole_response->end();
  }

  /**
   * {@inheritdoc}
   */
  public function error(\Throwable $e, DrupalKernel $kernel, Request $drupal_request, SwooleResponse $swoole_response): void {
    $swoole_response->header('Status', '500 Internal Server Error');
    $swoole_response->header('Content-Type', 'text/plain');

    // Uncomment the next line when you are debugging an error.
    // $swoole_response->end((string) $e);
    $swoole_response->end('Internal server error.');
  }

  /**
   * Get the HTTP reason clause for non-standard status codes.
   *
   * @param int $code
   *   The status code.
   *
   * @return string|null
   *   The code reason or NULL.
   */
  protected function getReasonFromStatusCode(int $code): ?string {
    if (array_key_exists($code, self::STATUS_CODE_REASONS)) {
      return self::STATUS_CODE_REASONS[$code];
    }

    return NULL;
  }

}
