<?php

namespace Drupal\swoole\Exceptions;

// cspell:ignore swoole

/**
 * The exception for task results.
 */
class TaskExceptionResult {

  /**
   * Constructs a TaskExceptionResult object.
   *
   * @param string $class
   *   The class name.
   * @param string $message
   *   The message.
   * @param int $code
   *   The code status.
   * @param string $file
   *   The file name.
   * @param int $line
   *   The line number.
   */
  public function __construct(
    protected string $class,
    protected string $message,
    protected int $code,
    protected string $file,
    protected int $line,
  ) {}

  /**
   * Creates a new task exception result from the given throwable.
   *
   * @param \Throwable $throwable
   *   The thrown exception.
   *
   * @return \Drupal\swoole\Exceptions\TaskExceptionResult
   */
  public static function from($throwable) {
    // @phpstan-ignore-next-line
    return new static(
      $throwable::class,
      $throwable->getMessage(),
      (int) $throwable->getCode(),
      $throwable->getFile(),
      (int) $throwable->getLine(),
    );
  }

  /**
   * Gets the original throwable.
   *
   * @return \Drupal\swoole\Exceptions\TaskException
   */
  public function getOriginal() {
    return new TaskException(
      $this->class,
      $this->message,
      (int) $this->code,
      $this->file,
      (int) $this->line,
    );
  }

}
