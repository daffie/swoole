<?php

namespace Drupal\swoole\Exceptions;

// cspell:ignore swoole

/**
 * Exception for value too large for column.
 */
class ValueTooLargeForColumnException extends \InvalidArgumentException {}
