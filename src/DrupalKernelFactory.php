<?php

namespace Drupal\swoole;

use Composer\Autoload\ClassLoader;
use Drupal\Core\DrupalKernel;
use Drupal\Core\Site\Settings;
use Swoole\Http\Server;

// cspell:ignore swoole Swoole

/**
 * The Drupal kernel factory.
 */
class DrupalKernelFactory {

  /**
   * DrupalKernelFactory constructor.
   *
   * @param \Composer\Autoload\ClassLoader $autoloader
   *   The class loader.
   * @param string $drupal_root
   *   The Drupal root directory.
   * @param string $site_path
   *   The Drupal site directory.
   * @param \Swoole\Http\Server $server
   *   The Swoole server.
   */
  public function __construct(
    protected ClassLoader $autoloader,
    protected string $drupal_root,
    protected string $site_path,
    protected Server $server
  ) {}

  /**
   * Create a new Drupal kernel instance.
   *
   * @return \Drupal\Core\DrupalKernel
   */
  public function createDrupalKernel(): DrupalKernel {
    $kernel = new DrupalKernel('prod', $this->autoloader, TRUE, $this->drupal_root);
    $kernel::bootEnvironment($this->drupal_root);
    $kernel->setSitePath($this->site_path);

    Settings::initialize($this->drupal_root, $this->site_path, $this->autoloader);

    $kernel->boot();

    // Add the Swoole server to the container of the Drupal kernel instance.
    $kernel->getContainer()->set('swoole_server', $this->server);

    return $kernel;
  }

}
