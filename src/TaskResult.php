<?php

namespace Drupal\swoole;

// cspell:ignore swoole

/**
 * The class that holds the task result.
 */
class TaskResult {

  /**
   * Constructs a TaskResult object.
   *
   * @param mixed $result
   *   The task result.
   */
  public function __construct(
    public mixed $result
  ) {}

}
