<?php

namespace Drupal\swoole;

use Symfony\Component\HttpFoundation\Request;
use Swoole\Http\Response as SwooleResponse;

// cspell:ignore swoole Swoole

/**
 * The worker interface that handles the user request.
 */
interface WorkerInterface {

  /**
   * Boot / initialize the Swoole worker.
   */
  public function boot(): void;

  /**
   * Handle an incoming request and send the response to the client.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The Drupal request.
   * @param \Swoole\Http\Response $swoole_response
   *   The Swoole response.
   * @param string $drupal_root
   *   Absolute path to the root of the Drupal installation.
   */
  public function handle(Request $request, SwooleResponse $swoole_response, string $drupal_root): void;

  /**
   * Handle an incoming task.
   *
   * @param mixed $data
   *   The data to be used by the task.
   *
   * @return mixed
   */
  public function handleTask($data);

  /**
   * Terminate the worker.
   */
  public function terminate(): void;

}
