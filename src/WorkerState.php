<?php

namespace Drupal\swoole;

// cspell:ignore swoole Swoole

/**
 * Holds the meta data for the worker process.
 */
class WorkerState {

  /**
   * The Swoole server.
   *
   * @var \Swoole\Http\Server
   */
  public $server;

  /**
   * The worker identifier.
   *
   * @var int
   */
  public $workerId;

  /**
   * The worker process identifier.
   *
   * @var int
   */
  public $workerPid;

  /**
   * The Swoole worker.
   *
   * @var \Drupal\swoole\Worker
   */
  public $worker;

  /**
   * The client.
   *
   * @var \Drupal\swoole\ClientInterface
   */
  public $client;

  /**
   * The timer table.
   *
   * @var array
   */
  public $timerTable;

  /**
   * The Drupal tables.
   *
   * @var array
   */
  public $tables = [];

  /**
   * The tick timer identifier.
   *
   * @var int
   */
  public $tickTimerId;

  /**
   * The latest request time.
   *
   * @var int
   */
  public $lastRequestTime;

}
