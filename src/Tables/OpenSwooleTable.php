<?php

namespace Drupal\swoole\Tables;

use Swoole\Table;

// cspell:ignore swoole

/**
 * The OpenSwoole memory table.
 */
class OpenSwooleTable extends Table {

  use EnsuresColumnSizesTrait;

  /**
   * The table columns.
   *
   * @var array
   */
  protected $columns;

  /**
   * Set the data type and size of the columns.
   *
   * @param string $name
   *   The column name.
   * @param int $type
   *   The type of the column.
   * @param int $size
   *   The size of the column.
   *
   * @return bool
   */
  public function column($name, $type, $size = 0): bool {
    $this->columns[$name] = [$type, $size];

    return parent::column($name, $type, $size);
  }

  /**
   * Update a row of the table.
   *
   * @param string $key
   *   The keys to be set.
   * @param array $values
   *   The values to be set.
   *
   * @return bool
   */
  public function set($key, array $values): bool {
    $this->ensureColumnsSize();

    return parent::set($key, $values);
  }

}
