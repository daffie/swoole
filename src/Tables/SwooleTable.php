<?php

namespace Drupal\swoole\Tables;

use Swoole\Table;

// cspell:ignore swoole Swoole SWOOLE

if (SWOOLE_VERSION_ID === 40804 || SWOOLE_VERSION_ID >= 50000) {

  /**
   * The Swoole memory table.
   */
  class SwooleTable extends Table {

    use EnsuresColumnSizesTrait;

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns;

    /**
     * Set the data type and size of the columns.
     *
     * @param string $name
     *   The column name.
     * @param int $type
     *   The type of the column.
     * @param int $size
     *   The size of the column.
     *
     * @return bool
     */
    public function column(string $name, int $type, int $size = 0): bool {
      $this->columns[$name] = [$type, $size];

      return parent::column($name, $type, $size);
    }

    /**
     * Update a row of the table.
     *
     * @param string $key
     *   The keys to be set.
     * @param array $values
     *   The values to be set.
     *
     * @return bool
     */
    public function set(string $key, array $values): bool {
      $this->ensureColumnsSize();

      return parent::set($key, $values);
    }

  }
}
else {

  /**
   * The Swoole memory table.
   */
  class SwooleTable extends Table {

    use EnsuresColumnSizesTrait;

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns;

    /**
     * Set the data type and size of the columns.
     *
     * @param string $name
     *   The column name.
     * @param int $type
     *   The type of the column.
     * @param int $size
     *   The size of the column.
     */
    public function column($name, $type, $size = 0): bool {
      $this->columns[$name] = [$type, $size];

      return parent::column($name, $type, $size);
    }

    /**
     * Update a row of the table.
     *
     * @param string $key
     *   The keys to be set.
     * @param array $values
     *   The values to be set.
     */
    public function set($key, array $values): bool {
      $this->ensureColumnsSize();

      return parent::set($key, $values);
    }

  }
}
