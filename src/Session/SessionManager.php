<?php

namespace Drupal\swoole\Session;

use Drupal\Core\Database\Connection;
use Drupal\Core\Session\MetadataBag;
use Drupal\Core\Session\SessionConfigurationInterface;
use Drupal\Core\Session\SessionManager as CoreSessionManager;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Proxy\SessionHandlerProxy;

// cspell:ignore swoole Swoole samesite

/**
 * Extends the core class SessionManager with asynchronous support.
 *
 * The regular SessionManager class does not work the Swoole extension, because
 * PHP is run in "cli" mode and cookies cannot be set with the regular function
 * setcookie(). The Swoole extension uses Swoole\Http\Response->cookie() or
 * Swoole\Http\Response->rawCookie() for setting cookies.
 *
 * @see: https://openswoole.com/docs/modules/swoole-http-response-cookie
 * @see: https://openswoole.com/docs/modules/swoole-http-response-rawCookie
 */
class SessionManager extends CoreSessionManager {

  /**
   * Array of cookies to be set.
   *
   * @var \Symfony\Component\HttpFoundation\Cookie[]
   */
  protected $cookies;

  /**
   * Whether Drupal is being used asynchronously.
   *
   * @var bool
   */
  protected $asynchronous;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, Connection $connection, MetadataBag $metadata_bag, SessionConfigurationInterface $session_configuration, $handler = NULL) {
    parent::__construct($request_stack, $connection, $metadata_bag, $session_configuration, $handler);

    $this->asynchronous = FALSE;
    // @phpstan-ignore-next-line
    if (\Drupal::getContainer()->has('swoole_server')) {
      $this->asynchronous = TRUE;
    }

    $this->cookies = [];
    // @phpstan-ignore-next-line
    $this->time = \Drupal::time();
  }

  /**
   * {@inheritdoc}
   */
  public function start(): bool {
    if (($this->started || $this->startedLazy) && !$this->closed) {
      return $this->started;
    }

    $request = $this->requestStack->getCurrentRequest();
    $this->setOptions($this->sessionConfiguration->getOptions($request));

    if ($this->sessionConfiguration->hasSession($request)) {
      // If a session cookie exists, initialize the session. Otherwise the
      // session is only started on demand in save(), making
      // anonymous users not use a session cookie unless something is stored in
      // $_SESSION. This allows HTTP proxies to cache anonymous pageviews.
      $result = $this->startNow();
    }

    if (empty($result)) {
      // Initialize the session global and attach the Symfony session bags.
      $_SESSION = [];
      $this->loadSession();

      // NativeSessionStorage::loadSession() sets started to TRUE, reset it to
      // FALSE here.
      $this->started = FALSE;
      $this->startedLazy = TRUE;

      $result = FALSE;
    }

    return $result;
  }

  /**
   * Forcibly start a PHP session.
   *
   * @return bool
   *   TRUE if the session is started.
   */
  protected function startNow() {
    if ($this->isCli() && !$this->asynchronous) {
      return FALSE;
    }

    if (!$this->asynchronous) {
      if ($this->startedLazy) {
        // Save current session data before starting it, as PHP will destroy it.
        $session_data = $_SESSION;
      }

      $result = NativeSessionStorage::start();

      // Restore session data.
      if ($this->startedLazy) {
        $_SESSION = $session_data;
        $this->loadSession();
      }
    }
    else {
      $session_data = NULL;
      if ($this->started) {
        $result = TRUE;
      }
      else {
        $request = $this->requestStack->getCurrentRequest();
        $cookie_name = $this->sessionConfiguration->getOptions($request)['name'] ?? '';
        if ($session_id = $request->cookies->get($cookie_name)) {
          $handler = $this->getSaveHandler() instanceof SessionHandlerProxy ? $this->getSaveHandler()->getHandler() : $this->getSaveHandler();
          $session_data = $handler->read($session_id);
          $session_data = $session_data ? unserialize($session_data) : NULL;
        }
      }

      $this->loadSession($session_data);

      $result = TRUE;
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if ($this->isCli() && !$this->asynchronous) {
      // We don't have anything to do if we are not allowed to save the session.
      return;
    }

    if ($this->isSessionObsolete()) {
      // There is no session data to store, destroy the session if it was
      // previously started.
      if ($this->getSaveHandler()->isActive()) {
        $this->destroy();
      }
    }
    else {
      // There is session data to store. Start the session if it is not already
      // started.
      if (!$this->getSaveHandler()->isActive()) {
        $this->startNow();
      }

      // Store a copy so we can restore the bags in case the session was not left empty
      $session = $_SESSION;

      foreach ($this->bags as $bag) {
        if (empty($_SESSION[$key = $bag->getStorageKey()])) {
          unset($_SESSION[$key]);
        }
      }
      if ([$key = $this->metadataBag->getStorageKey()] === array_keys($_SESSION)) {
        unset($_SESSION[$key]);
      }

      if (!$this->asynchronous) {
        // Register error handler to add information about the current save handler
        $previousHandler = set_error_handler(function ($type, $msg, $file, $line) use (&$previousHandler) {
          if (\E_WARNING === $type && str_starts_with($msg, 'session_write_close():')) {
            $handler = $this->saveHandler instanceof SessionHandlerProxy ? $this->saveHandler->getHandler() : $this->saveHandler;
            $msg = sprintf('session_write_close(): Failed to write session data with "%s" handler', \get_class($handler));
          }

          return $previousHandler ? $previousHandler($type, $msg, $file, $line) : FALSE;
        });

        try {
          session_write_close();
        } finally {
          restore_error_handler();

          // Restore only if not empty
          if ($_SESSION) {
            $_SESSION = $session;
          }
        }
      }
      else {
        $request = $this->requestStack->getCurrentRequest();
        $cookie_name = $this->sessionConfiguration->getOptions($request)['name'];
        $cookie_lifetime = $this->sessionConfiguration->getOptions($request)['cookie_lifetime'];
        $cookie_expire = $this->time->getRequestTime() + $cookie_lifetime;
        $cookie_data = session_get_cookie_params();
        // Use the CSRF Token as the session ID.
        $session_id = $this->getMetadataBag()->getCsrfTokenSeed();

        $this->cookies[] = new Cookie(
          $cookie_name,
          $session_id,
          $cookie_expire,
          $cookie_data['path'],
          $cookie_data['domain'],
          $cookie_data['secure'],
          $cookie_data['httponly'],
          FALSE,
          (string) $cookie_data['samesite']
        );

        $handler = $this->getSaveHandler() instanceof SessionHandlerProxy ? $this->getSaveHandler()->getHandler() : $this->getSaveHandler();
        $handler->write($session_id, serialize($_SESSION));

        // Restore only if not empty
        if ($_SESSION) {
          $_SESSION = $session;
        }
      }

      $this->closed = TRUE;
      $this->started = FALSE;
    }

    $this->startedLazy = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function regenerate($destroy = FALSE, $lifetime = NULL): bool {
    // Nothing to do if we are not allowed to change the session.
    if ($this->isCli() && !$this->asynchronous) {
      return FALSE;
    }

    // Drupal will always destroy the existing session when regenerating a
    // session. This is inline with the recommendations of @link https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html#renew-the-session-id-after-any-privilege-level-change
    // OWASP session management cheat sheet. @endlink
    $destroy = TRUE;

    // Cannot regenerate the session ID for non-active sessions.
    if (\PHP_SESSION_ACTIVE !== session_status()) {
      // Ensure the metadata bag has been stamped. If the
      // NativeSessionStorage::regenerate() is called prior to the session being
      // started it will not refresh the metadata as expected.
      $this->getMetadataBag()->stampNew($lifetime);
      return FALSE;
    }

    return NativeSessionStorage::regenerate($destroy, $lifetime);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($uid) {
    // Nothing to do if we are not allowed to change the session.
    if (!$this->writeSafeHandler->isSessionWritable() || ($this->isCli() && !$this->asynchronous)) {
      return;
    }
    $this->connection->delete('sessions')
      ->condition('uid', $uid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function destroy() {
    if ($this->isCli() && !$this->asynchronous) {
      return;
    }

    // Symfony suggests using Session::invalidate() instead of session_destroy()
    // however the former calls session_regenerate_id(TRUE), which while
    // destroying the current session creates a new ID; Drupal has historically
    // decided to only set sessions when absolutely necessary (e.g., to increase
    // anonymous user cache hit rates) and as such we cannot use the Symfony
    // convenience method here.
    session_destroy();

    // Unset the session cookies.
    $session_name = $this->getName();
    $cookies = $this->requestStack->getCurrentRequest()->cookies;
    // setcookie() can only be called when headers are not yet sent.
    if ($cookies->has($session_name) && !headers_sent()) {
      $params = session_get_cookie_params();
      if (!$this->asynchronous) {
        setcookie($session_name, '', $this->time->getRequestTime() - 3600, $params['path'], $params['domain'], $params['secure'], $params['httponly']);
      }
      else {
        $this->cookies[] = new Cookie(
          $session_name,
          '',
          $this->time->getRequestTime() - 3600,
          $params['path'],
          $params['domain'],
          $params['secure'],
          $params['httponly']
        );
      }
      $cookies->remove($session_name);
    }
  }

  /**
   * Add the cookies to be set to the Drupal response.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The Drupal response.
   */
  public function addCookiesToResponse(Response &$response) {
    foreach ($this->cookies as $cookie) {
      $response->headers->setCookie($cookie);
    }
  }

  /**
   * Reset session after the Drupal response.
   */
  public function resetSessionAfterResponse() {
    $this->cookies = [];
    $this->clear();
  }

}
