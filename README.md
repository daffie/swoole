
<p align="center">
<a href="https://packagist.org/packages/daffie/swoole"><img src="https://img.shields.io/packagist/dt/daffie/swoole" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/daffie/swoole"><img src="https://img.shields.io/packagist/v/daffie/swoole" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/daffie/swoole"><img src="https://img.shields.io/packagist/l/daffie/swoole" alt="License"></a>
</p>

## Introduction

The Drupal Swoole module supercharges your Drupal website performance by serving your website using high-powered Swoole servers [Open Swoole](https://swoole.co.uk) or [Swoole](https://github.com/swoole/swoole-src). Swoole boots Drupal once, keeps it in memory, and then feeds it requests at supersonic speeds.

## Official Documentation

Documentation for Drupal on Swoole can be found on the [Daffie's website](https://daffie.gitlab.io/swoole/).

## Contributing

Thank you for considering contributing to the Swoole module! Merge requests and bugs reports are more then welcome.

## License

The Swoole module is open-sourced software licensed under the [GPL v2 or later](LICENSE.md).

## Origin

This module is based on the [Laravel Octane](https://github.com/laravel/octane). Thank you Taylor Otwell and friends for that.
